
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Cars - CPSC 2030</title>
  </head>
  
  <body class="container">
    <h1>Cars - CPSC 2030</h1>
    
    <form action="logout.php" ><button class="btn btn-primary btn-lg btn-block" type="submit">Logout</button> </form>

    <form class="needs-validation" novalidate method="POST" action="" >
      <div class="row">
        <div class="col-md-6 mb-3">
          <label for="make">Make</label>
            <input type="text" class="form-control" id="make" name="make" placeholder="" value="" required name="make">
              <div class="invalid-feedback">
                Valid make is required.
              </div>
        </div>
          
        <div class="col-md-6 mb-3">
          <label for="model">Model</label>
            <input type="text" class="form-control" id="model"name="model"placeholder="" value="" required name="model">
              <div class="invalid-feedback">
                Valid make is required.
              </div>
        </div>
      </div>
        
      <div class="row">
        <div class="col-md-6 mb-3">
          <label for="year">Year</label>
            <input type="year" class="form-control" id="year" name="year" placeholder="" value="" required name="year">
              <div class="invalid-feedback">
                Valid year is required.
          </div>
        </div>
          
        <div class="col-md-6 mb-3">
          <label for="mileage">Mileage</label>
            <input type="number" class="form-control" id="mileage" name ="mileage" placeholder="" value="" required name="mileage">
              <div class="invalid-feedback">
                Valid mileage is required.
              </div>
        </div>
      </div>
        
    <button class="btn btn-primary btn-lg btn-block" type="submit">Add car</button>

    <table class="table">
        <thead>
            <tr>
              <th scope="col">ID</th>
              <th scope="col">Make</th>
              <th scope="col">Model</th>
              <th scope="col">Year</th>
              <th scope="col">Mileage</th>
    
            </tr>
        </thead>
      <tbody>
        
    <?php 
      
      session_start();  
      
      $link = mysqli_connect( 'localhost', 'root', 'himanshu' );
      mysqli_select_db( $link, 'Demo' );
      if($_SESSION['use']){    
        if ( isset( $_REQUEST["make"] ) ) {
          $safe_make = mysqli_real_escape_string( $link, $_REQUEST["make"] );
          $safe_model = mysqli_real_escape_string( $link, $_REQUEST["model"] );
          $safe_year= mysqli_real_escape_string( $link, $_REQUEST["year"] );
          $safe_mileage= mysqli_real_escape_string( $link, $_REQUEST["mileage"] );
                
          $query = "INSERT INTO Cars ( Make, Model, Year, Mileage ) VALUES ( '$safe_make', '$safe_model',  $safe_year, $safe_mileage)";

          mysqli_query( $link, $query );
        }
            
        $results = mysqli_query( $link, 'SELECT * FROM Cars' );
        // process $results
        while( $record = mysqli_fetch_assoc( $results) ) {
          $id = $record['ID'];
          $make = $record['Make'];
          $model = $record['Model'];
          $year = $record['Year'];
          $mileage = $record['Mileage'];
         
          print "<tr> <td>$id</td> <td>$make</td> <td>$model</td> <td>$year</td> <td>$mileage</td> ".
          '<td> <form action="delete.php" method="post">
          <input type="hidden" name="id" value="' . $record['ID'] . '" >
          <input  type="submit" id="'.$record['ID'].'" value="Delete"></form></td> '." </tr>";   

        }
      }
      
      else{
        
      }
      mysqli_free_result( $results );
      mysqli_close( $link );
        
    ?>

      </tbody>
      </table>
    
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    
  </body>
</html>
