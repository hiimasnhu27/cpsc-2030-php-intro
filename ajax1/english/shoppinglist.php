<?php
    header( "Content-type: application/json");
    
    $link = mysqli_connect( 'localhost', 'root', 'himanshu' );
    if ( ! $link ) {
      $error_number = mysqli_connect_errno();
      $error_message = mysqli_connect_error();
      file_put_contents( "/tmp/ajax.log", "($error_number) $error_message\n", FILE_APPEND );
      http_response_code( 500 );
      exit(1);
    }
    $dbName = "Demo";
    if ( ! mysqli_select_db( $link, $dbName ) ) {
      $error_number = mysqli_errno( $link );
      $error_message = mysqli_error( $link );
      file_put_contents( "/tmp/ajax.log", "($error_number) $error_message\n", FILE_APPEND );
      http_response_code( 500 );
      exit(1);
    }
    
    switch ( $_SERVER['REQUEST_METHOD']) {
        case 'GET':
            $results = mysqli_query( $link, 'select * from Cars' );
            
            if ( ! $results ) {
                $error_number = mysqli_errno( $link );
                $error_message = mysqli_error( $link );
                file_put_contents( "/tmp/ajax.log", "($error_number) $error_message\n", FILE_APPEND );
                http_response_code( 500 );
                exit(1);
            } else {
                $shoppinglist = array();
                while( $record = mysqli_fetch_assoc( $results ) ) {
                    $shoppinglist[] = $record;
                }
                mysqli_free_result( $results );
                echo json_encode( $shoppinglist );
            }
            break;
        
        case 'POST':
            $safe_make = mysqli_real_escape_string( $link, $_REQUEST["Make"] );
            echo $safe_make;
            $safe_model = mysqli_real_escape_string( $link, $_REQUEST["Model"] );
            echo $safe_model;
            $year = $_REQUEST["Year"] + 0;
            echo $year;
            $mileage = $_REQUEST["Mileage"] + 0;
            echo $mileage;
            if ( strlen( $safe_make ) <= 0 ||
                 strlen( $safe_make ) > 80 ||
                 strlen( $safe_model ) <= 0 ||
                 strlen( $safe_model ) > 80 ||
                 $year <= 0 ||
                 $mileage <= 0 ){
                file_put_contents( "/tmp/ajax.log", "Invalid Client Request\n", FILE_APPEND );
                http_response_code( 400 );
                exit(1);
            }

            $query = "INSERT INTO Cars ( Make, Model, Year, Mileage ) VALUES( '$safe_make', '$safe_model', $year, $mileage )";
            if ( ! mysqli_query( $link, $query ) ) {
              $error_number = mysqli_errno( $link );
              $error_message = mysqli_error( $link );
              file_put_contents( "/tmp/ajax.log", "($error_number) $error_message\n", FILE_APPEND );
              http_response_code( 500 );
            }
            break;
        
        case 'DELETE':
            
            $ID = $_REQUEST['ID'] + 0;
            $query1 = "delete from Cars where ID = $ID";
            
            if ( ! mysqli_query( $link, $query1 ) ) {
              $error_number = mysqli_errno( $link );
              $error_message = mysqli_error( $link );
              file_put_contents( "/tmp/ajax.log", "($error_number) $error_message\n", FILE_APPEND );
              http_response_code( 500 );
            }
            
            break;


    }
